q = (query, all=false) ->
    if all
        document.querySelectorAll(query)
    else
        document.querySelector(query)

render_template = (template, data=this) ->
    Haml.render(template, {'context': data})


init_game = () ->
    @cards_row_count = 3

    categories_count = (q("#categorie_#{i}").valueAsNumber for i in [0..categories.length - 1])

    team_color_offset = Math.random()

    window.game_data =
        classes: [q('#class > input:nth-of-type(1)').valueAsNumber..q('#class > input:nth-of-type(2)').valueAsNumber]
        time_multi: q('#time > input').valueAsNumber / 100
        teams: ({color: "hsl(#{1 / q('#team > input').valueAsNumber * i - 1 / q('#team > input').valueAsNumber + team_color_offset}turn, 100%, 60%)", points: 0} for i in [0..q('#team > input').valueAsNumber - 1])
        active_team: 0
        active_retry_team: 0
        columns: []

    for val, i in categories_count
        if val > 0
            for j in [0..val - 1]
                window.game_data.columns.push(i)

    possible_cards = (i for i in [0..cards.length - 1] when cards[i]['class'] in window.game_data.classes)

    window.game_data.cards = []
    for x in [0..window.game_data.columns.length - 1]
        window.game_data.cards[x] = []
        for y in [0..@cards_row_count-1]
            sub_possible_cards = (i for i in possible_cards when cards[i].categorie == categories[window.game_data.columns[x]] and cards[i].difficulty == y + 1)

            window.game_data.cards[x][y] = null
            if sub_possible_cards.length > 0
                window.game_data.cards[x][y] = sub_possible_cards[Math.floor(Math.random() * sub_possible_cards.length)]

                possible_cards.splice(possible_cards.indexOf(window.game_data.cards[x][y]), 1)

    range_x = [0..window.game_data.cards.length-1]
    range_y = [window.game_data.cards[0].length-1..0]
    range_y.unshift(-1)

    q('#game').innerHTML = render_template("""
%div#cards_wrapper
    :each _, y in this.range_y
        :if y == -1
            %div.difficulty
        :else
            %div.difficulty= (parseInt(y) + 1) * 10
        %div.row
            :each _, x in this.range_x
                :if y == -1
                    %div.headline= categories[window.game_data.columns[x]]
                :else
                    :if window.game_data.cards[x][y] === null
                        %div.card.disabled{x: x, y: y}
                    :else
                        %div.card{x: x, y: y, id: window.game_data.cards[x][y]}
    """, {game_data: window.game_data, range_x: range_x, range_y: range_y})

    for card in q('.card', all=true)
        card.onclick = show_card

    update_teams()

    q('#selector').classList.remove('active')
    q('#game').classList.add('active')
    q('#teams').classList.add('active')


update_teams = () ->
    if q('.team') == null
        q('#teams').innerHTML = render_template("""
:each i, team in window.game_data.teams
    %div.team{id: "team_" + i, style: "background: " + team.color}
        %span.points= team.points
        """, {game_data: window.game_data})
    else
        for team, i in window.game_data.teams
            q('.team .points', all=true)[i].innerHTML = team.points
            q('.team', all=true)[i].classList.remove('active')

    active_team = window.game_data.active_team
    if window.game_data.active_retry_team != null
        active_team = window.game_data.active_retry_team

    if active_team != null
        q('.team', all=true)[active_team].classList.add('active')


show_card = (e, id=null, repeat=false) ->
    if id == null
        id = parseInt(this.id)

    card = window.cards[id]
    
    if repeat
        window.fail_cycle_count += 1
    else
        window.fail_cycle_count = 1


    next_team = () ->
        q('#answer input[type="text"]').value = null
        q('#answer input[type="text"]').disabled = true
        q('#answer input[type="button"]').disabled = true

        q('#answer input[type="text"]').blur()

        window.game_data.active_retry_team = (window.game_data.active_retry_team + 1) % window.game_data.teams.length
        update_teams()

        if window.fail_cycle_count >= window.game_data.teams.length
            disable_card()
        else
            setTimeout(() ->
                show_card(null, id, repeat=true)
            , 3000)


    disable_card = () ->
        window.game_data.active_team = (window.game_data.active_team + 1) % window.game_data.teams.length
        window.game_data.active_retry_team = window.game_data.active_team
        update_teams()

        show_pattern_solution(null, id, callback=() ->
            q(".card[id=\"#{id}\"").classList.add('played')
            q(".card[id=\"#{id}\"").onclick = null

            if q('.card:not(.disabled):not(.played)') == null
                win()
        )


    set_countdown((card.time * 1000) * window.game_data.time_multi, true, next_team)

    q('#answer input[type="button"]').onclick = () ->
        set_countdown(0, false, null)

        if parseFloat(q('#answer input[type="text"]').value.replace(/\,/g, '.')) == card.solution
            window.game_data.teams[window.game_data.active_retry_team].points += card.difficulty * 10

            q(".card[id=\"#{id}\"").style.borderColor = window.game_data.teams[window.game_data.active_retry_team].color

            disable_card()
        else
            next_team()

    q('#answer input[type="text"]').value = null
    q('#answer input[type="text"]').disabled = false
    q('#answer input[type="button"]').disabled = false
    q('#card .close_button').disabled = true

    q('#card img').src = card.description

    q('#card_wrapper').classList.add('active')

    q('#answer input[type="text"]').focus()

    q('#answer input[type="text"]').onkeyup = q('#answer input[type="text"]').onkeydown = (e) ->
        # this.value = this.value.replace(/[^0-9-.,]/, '')  # Fluff JS

        if e.keyCode == 13
            q('#answer input[type="button"]').click()


show_pattern_solution = (e, id=null, callback=()->) ->
    if id == null
        id = parseInt(this.id)
    
    card = window.cards[id]

    q('#answer input[type="text"]').value = null
    q('#answer input[type="text"]').disabled = true
    q('#answer input[type="button"]').disabled = true
    q('#card .close_button').disabled = false

    q('#answer input[type="text"]').blur()
    
    q('#card img').src = card.pattern_solution

    q('#card_wrapper').classList.add('pattern_solution')

    close_card = () ->
        document.onkeydown = null

        q('#card_wrapper').classList.remove('active')
        q('#card_wrapper').classList.remove('pattern_solution')

        callback()

    q('#card .close_button').onclick = close_card
    document.onkeydown = (e) ->
        if e.keyCode == 27
            close_card()


win = () ->
    winning_teams = {}
    for i, team of window.game_data.teams
        winning_teams[team.points] = []

    for i, team of window.game_data.teams
        winning_teams[team.points].push(i)

    heighest_points = Object.keys(winning_teams).sort((a, b) -> 
        parseFloat(b) - parseFloat(a)
    )

    q('#win > div').innerHTML = render_template("""
:each i, points in this.heighest_points
    %div.row
        %span.place= (parseInt(i) + 1) + '.'
        %span.points= points
        :each j, team in this.winning_teams[points]
            %div.team{style: "background-color: " + window.game_data.teams[team].color + ";border-color: " + window.game_data.teams[team].color}
            """, {heighest_points: heighest_points, winning_teams: winning_teams})

    q('#win_wrapper').classList.add('active')

    close_win = () ->
        document.onkeydown = null

        q('#win_wrapper').classList.remove('active')

    q('#win > .close_button').onclick = close_win

    document.onkeydown = (e) ->
        if e.keyCode == 27
            close_win()

    window.game_data.active_team = null
    window.game_data.active_retry_team = null
    update_teams()

    q('#game > #cards_wrapper').classList.add('played')

    q('#game > #cards_wrapper .card', all=true).forEach((card) ->
        card.onclick = show_pattern_solution
    )

    window.confetti.start()


set_countdown = (time, active, callback) ->
    if active
        q('#countdown').classList.add('active')
    else
        q('#countdown').classList.remove('active')

    @countdown_data.start_time = time

    @countdown_data.time = Date.now() + time

    @countdown_data.callback = callback

    if active
        if @countdown_data.interval == null
            @countdown_data.interval = setInterval(
                () ->
                    if @countdown_data.time - Date.now() <= 0
                        q('#countdown_overlay').classList.add('active')
                        setTimeout(() ->
                            q('#countdown_overlay').classList.remove('active')
                        , 500)
                        
                        @countdown_data.callback()

                        set_countdown(0, false, null)

                        return

                    time_floor = Math.floor((@countdown_data.time - Date.now()) / 1000)
                    if ((@countdown_data.time - Date.now()) / 1000) - time_floor < 0.5 and (time_floor == 0 or time_floor == 1 or time_floor == 2 or time_floor == 3 or time_floor == 7 or time_floor == 10)
                        q('#countdown_overlay').classList.add('active')
                    else
                        q('#countdown_overlay').classList.remove('active')

                    q('#countdown div').style.width = ((1 - ((@countdown_data.time - Date.now()) / @countdown_data.start_time)) * 100) + '%'
            , 100)
    else
        if @countdown_data.interval != null
            clearInterval(@countdown_data.interval)
            @countdown_data.interval = null


test = () ->
    window.update_teams = update_teams

    q("#categorie_0").value = 0
    q("#categorie_1").value = 1

    q('#time > input').value = 100

    q("#team > input").value = 3

    q('#start').click()


window.onload = () ->
    @countdown_data = {
        'time': 0,
        'start_time': 0,
        'interval': null,
        'callback': null
    }

    q('#start').onclick = init_game

    # if not window.debug
    #     threshold = 160
    #     minimalUserResponseInMiliseconds = 100;
    #     setInterval(() ->
    #         console.clear()

    #         widthThreshold = window.outerWidth - window.innerWidth > threshold
    #         heightThreshold = window.outerHeight - window.innerHeight > threshold

    #         before = Date.now()
    #         debugger
    #         after = Date.now()

    #         if (after - before > minimalUserResponseInMiliseconds) or (not (heightThreshold and widthThreshold) and ((window.Firebug and window.Firebug.chrome and window.Firebug.chrome.isInitialized) or widthThreshold or heightThreshold))
    #             alert('Cheater!')
    #             location.reload(true)
    #     , 200)

    # if window.debug
        # test()
