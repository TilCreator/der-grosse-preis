@set_card = (id) ->
    data = {}
    data.id = id
    data.name = document.querySelector('#name').value
    if document.querySelector('#description_b64').value.length > 10
        data.description = document.querySelector('#description_b64').value
    if document.querySelector('#pattern_solution_b64').value.length > 10
        data.pattern_solution = document.querySelector('#pattern_solution_b64').value
    data.categorie = document.querySelector('#categorie').value
    data.solution = parseFloat(document.querySelector('#solution').value)
    data.class = parseFloat(document.querySelector('#class').value)
    data.difficulty = parseFloat(document.querySelector('#difficulty').value)
    data.time = parseFloat(document.querySelector('#time').value)

    x = new XMLHttpRequest()
    x.open("POST", "/ajax/set_card")
    x.setRequestHeader("Content-Type", "application/json")
    x.onreadystatechange = () ->
        if x.readyState == 4
            data = JSON.parse(x.responseText)
            if 'error' of data
                alert("ERROR: #{data.error}")
            else
                alert("Gespeichert")

            if 'redirect' of data
                window.open(data.redirect, '_self')
            else
                location.reload()
    x.send(JSON.stringify(data))


@rm_card = (id) ->
    if confirm('Wirklich löschen ?')
        x = new XMLHttpRequest()
        x.open("POST", "/ajax/rm_card")
        x.setRequestHeader("Content-Type", "application/json")
        x.onreadystatechange = () ->
            if x.readyState == 4
                data = JSON.parse(x.responseText)
                if 'error' of data
                    alert("ERROR: #{data.error}")
                else
                    alert("Gelöscht")

                if 'redirect' of data
                    window.open(data.redirect, '_self')
        x.send(JSON.stringify({'id': id}))


@img_to_b64 = (name) ->  # https://stackoverflow.com/a/17711190
    FR = new FileReader()
    FR.addEventListener('load', (e) ->
        document.querySelector("##{name}_prev_img").src = e.target.result;
        document.querySelector("##{name}_b64").value = e.target.result;
    )
    FR.readAsDataURL(document.querySelector("##{name}").files[0])


@get_curriculum = (data) ->
    document.querySelector('#curriculum').value = ''

    keys = Object.keys(data)
    keys.sort((a, b) ->
        if parseFloat(a) > parseFloat(b)
            return 1
        if parseFloat(a) < parseFloat(b)
            return -1
        return 0
    )


    for _, key of keys
        document.querySelector('#curriculum').value += data[key] + '\n'


@set_curriculum = () ->
    data = {}
    data_raw = document.querySelector('#curriculum').value.split('\n')
    for i of data_raw
        line_raw = data_raw[i]

        if line_raw.length > 1
            match = line_raw.match(' *([0-9\.]+) *(.*)')

            if match == null
                alert('Invalider Lehrplan\nFormat: "X(.X(.X)) Name" bsp. "1.1 Binärsystem"')
                return

            number_match = (match[1] + '.').match('([0-9]+)([0-9\.]*)')
            console.log number_match
            if number_match[2] == '.'
                number = number_match[1]
            else
                number = number_match[1] + '.' + number_match[2].split('.').join('')

            data[number] = match[1] + ' ' + match[2]


    x = new XMLHttpRequest()
    x.open("POST", "/ajax/set_curriculum")
    x.setRequestHeader("Content-Type", "application/json")
    x.onreadystatechange = () ->
        if x.readyState == 4
            data = JSON.parse(x.responseText)
            if 'error' of data
                alert("ERROR: #{data.error}")
            else
                alert("Gespeichert")

            if 'redirect' of data
                window.open(data.redirect, '_self')
    x.send(JSON.stringify(data))
