from flask import Flask, render_template, request, abort
from flask_basicauth import BasicAuth
try:
    from flask_cake import Cake
except Exception:
    pass
from copy import copy
import dataset
import base64
import json
import os
import re

import config


curriculum = {}
if os.path.exists('curriculum.json'):
    with open('curriculum.json', 'r') as f:
        curriculum = json.load(f)


app = Flask(__name__)

try:
    cake = Cake(app)
except Exception:
    pass

def base64_encode_file(path):
    # Does **no** sanetizing!
    ext = path[-3:]
    with open(path[1:], "rb") as file:
        return f'data:image/{ext};base64,' + base64.b64encode(file.read()).decode()

app.jinja_env.add_extension('hamlish_jinja.HamlishExtension')
app.jinja_env.globals.update(base64=base64_encode_file)
if config.debug:
    app.jinja_env.hamlish_mode = 'debug'


app.db = dataset.connect(config.db_url)
app.db.create_table('cards')


app.config['BASIC_AUTH_USERNAME'] = config.username
app.config['BASIC_AUTH_PASSWORD'] = config.password

basic_auth = BasicAuth(app)


def get_available_id(table):
    latest = table.find_one(order_by='-id')

    if latest is None:
        return 1

    return latest['id'] + 1


@app.template_global()
def include_static(filename):
    fullpath = os.path.join(app.static_folder, filename)
    with open(fullpath, 'r') as f:
        return f.read()


@app.route('/')
def index():
    return render_template('index.haml')


@app.route('/dashboard')
@basic_auth.required
def dashboard():
    def lenght_bytes_formated(data):
        def sizeof_fmt(num, suffix='B'):
            if not isinstance(num, str):
                for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
                    if abs(num) < 1024.0:
                        return "%3.1f%s%s" % (num, unit, suffix)
                    num /= 1024.0
                return str("%.1f%s%s" % (num, 'Yi', suffix))
            else:
                return str(num)

        return sizeof_fmt(len(data))

    return render_template('dashboard.haml', cards=app.db['cards'].find(order_by=['class', '-categorie', 'difficulty', '-name']),
                           curriculum=json.dumps(curriculum),
                           len=len, lenght_bytes_formated=lenght_bytes_formated)


@app.route('/editor')
@basic_auth.required
def editor_redir():
    return render_template('editor.haml', card={
                                                'name': 'Neue Karte',
                                                'description': '',
                                                'pattern_solution': '',
                                                'categorie':  '',
                                                'solution': 42,
                                                'difficulty': 1,
                                                'class': 5.0,
                                                'time': 30
                                                })


@app.route('/editor/<int:id>')
@basic_auth.required
def editor(id):
    if not app.db['cards'].find_one(id=id):
        abort(404)

    return render_template('editor.haml', card=app.db['cards'].find_one(id=id))


@app.route('/game')
@app.route('/download')
def game():
    download = 'download' in request.path

    categories = list(dict.fromkeys([row['categorie'] for row in app.db.query('SELECT categorie FROM cards WHERE LENGTH(description) > 10 AND LENGTH(pattern_solution) > 10')]))
    categories_classes = copy(categories)
    for i, categorie in enumerate(categories_classes):
        categories_classes[i] += f' ['

        for c_class in range(1, 14):
            count = app.db['cards'].count(categorie=categorie, **{'class': c_class})

            if count > 0:
                categories_classes[i] += f'M{c_class:02}, '

        categories_classes[i] = f'{categories_classes[i][:-2]}]'

    return render_template('game.haml', download=download, debug=config.debug,
                           cards_json=json.dumps(list(app.db.query('SELECT * FROM cards WHERE LENGTH(description) > 10 AND LENGTH(pattern_solution) > 10 ORDER BY class, name DESC'))),
                           curriculum_json=json.dumps(curriculum), categories_enum=dict(enumerate(categories_classes)),
                           categories_json=json.dumps(categories))


@app.route('/ajax/set_card', methods=['POST'])
def set_card():
    raw_data = json.loads(request.data)
    data = {}

    for key in ['id', 'name', 'description', 'solution', 'difficulty', 'time', 'class', 'categorie', 'pattern_solution']:
        if key not in raw_data:
            if key in ['id', 'description', 'pattern_solution']:
                if key in ['description', 'pattern_solution']:
                    if 'id' not in raw_data or len(app.db['cards'].find_one(id=raw_data['id'])[key]) < 10:
                        data[key] = ''
                continue
            return json.dumps({'error': f'KeyError: {key}'}), 400
        elif key in ['id']:
            if not isinstance(raw_data[key], int):
                return json.dumps({'error': f'TypeError: {key}'}), 400
            if not app.db['cards'].find_one(id=raw_data[key]):
                return json.dumps({'error': f'KeyError: {key}'}), 400
        elif key in ['difficulty', 'class', 'time', 'solution']:
            if not isinstance(raw_data[key], float) and not isinstance(raw_data[key], int):
                return json.dumps({'error': f'TypeError: {key}'}), 400
        elif key in ['name', 'description', 'pattern_solution', 'categorie']:
            if not isinstance(raw_data[key], str):
                return json.dumps({'error': f'TypeError: {key}'}), 400

        data[key] = raw_data[key]

    if 'id' in data:
        app.db['cards'].update(data, ['id'])
    else:
        id = app.db['cards'].insert(data)

        return json.dumps({'redirect': f'/editor/{id}'})

    return json.dumps({})


@app.route('/ajax/set_curriculum', methods=['POST'])
def set_curriculum():
    global curriculum
    data = json.loads(request.data)

    for key, value in data.items():
        if key.count('.') > 1 or not re.match(r'[0-9\.]+', key):
            return json.dumps({'error': f'KeyError: Invalid key'}), 400

    curriculum = data
    with open('curriculum.json', 'w') as f:
        json.dump(curriculum, f, indent=4)

    return json.dumps({})


@app.route('/ajax/rm_card', methods=['POST'])
def rm_card():
    raw_data = json.loads(request.data)

    if 'id' not in raw_data or not app.db['cards'].find_one(id=raw_data['id']):
        return json.dumps({'error': 'KeyError: id'}), 400

    app.db['cards'].delete(id=raw_data['id'])

    return json.dumps({'redirect': '/dashboard'})


if __name__ == '__main__':
    app.run(config.host, config.port, debug=config.debug)
