# Der Große Preis
![Logo](logo.svg)  
Logo by [Aja-mi](https://aja-mi.deviantart.com/)
# Install
* Get Python 3
* Get Coffeescript
* `$ git clone https://gitlab.com/TilCreator/der-grosse-preis`
* `$ cd der-grosse-preis`
* `$ python3 -m venv venv`
* `$ venv/bin/pip install -r requirements.txt`
* `$ venv/bin/python index.py`
## Advanced
Service file for systemd and ini for uwsgi available.
